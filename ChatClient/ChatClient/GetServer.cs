﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace ChatClient
{
    public partial class GetServer : Form
    {
        public static IPAddress serv;
        public static int port;

        public GetServer()
        {
            InitializeComponent();
            //this.AcceptButton = button1;
            //this.CancelButton = button2;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                serv = IPAddress.Parse(textBox1.Text.ToString());
                if (String.IsNullOrWhiteSpace(textBox2.Text.ToString()) || textBox2.Text=="")
                    MessageBox.Show("Invalid port number.", "Error!");
                else
                {
                    port = Int32.Parse(textBox2.Text.ToString());
                    if (port <= 1023 || port >= 65535)
                        MessageBox.Show("Invalid port number.", "Error!");
                    else
                        this.Close();
                }
            }
            catch(FormatException)
            {
                MessageBox.Show("Invalid IP Address.", "Error!");
            }
        }
    }
}
