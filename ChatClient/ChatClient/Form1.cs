﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ChatClient
{
    public partial class Form1 : Form
    {
        private string name="",data;
        private Socket client=null;
        private int port=0;
        private Thread sendt, recvt;
        private IPAddress serv;
        private bool DataEntered = false;

        public Form1()
        {
            InitializeComponent();
//            Icon i=new Icon(@"D:\Icons\FauxS-XP(Onyx)V3 Icon 03.ico");
//            this.Icon = i;
            this.AcceptButton = button1;
            this.disconnectToolStripMenuItem.Enabled = false;
            textBox2.LostFocus+=new EventHandler(textBox2_LostFocus);
            textBox1.Focus();
        }

        private void textBox2_LostFocus(object sender, EventArgs e)
        {
            if (name != textBox2.Text && !string.IsNullOrEmpty(textBox2.Text))
            {
                name = textBox2.Text.ToString();
                listBox1.Items.Add("Name changed to " + name);
            }
        }

        private void Send()
        {
//            client.Send(Encoding.ASCII.GetBytes(name)); 
            while (true)
            {
                if (DataEntered)
                {
                    data=textBox1.Text;
                    DataEntered = false;
                    client.Send(Encoding.ASCII.GetBytes(name + ": " + textBox1.Text));
                    listBox1.Items.Add(name + ": " + data);
                    textBox1.Clear();
                }
            }
        }

        private void Receive()
        {
            byte[] buf = new byte[1024];
            int n;
/*            client.Receive(buf);
            servername = Encoding.ASCII.GetString(buf);
            if (string.IsNullOrWhiteSpace(servername) || servername == "")
                servername = "Server";*/
            do
            {
                n=client.Receive(buf);
                data=Encoding.ASCII.GetString(buf,0,n);
                if(data.IndexOf("QUIT")<0)
                    listBox1.Items.Add(data);    
            }while(data.IndexOf("QUIT")<0);
            disconnectToolStripMenuItem_Click("this", new EventArgs());
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            byte[] buf = new byte[50];
            GetServer gs = new GetServer();
            //if (DialogResult.Cancel != gs.ShowDialog())
            //MessageBox.Show(gs.ShowDialog().ToString());
            gs.ShowDialog();
            {
                try
                {
                    port = GetServer.port;
                    serv = GetServer.serv;

                    client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    client.Connect(serv, port);
                    disconnectToolStripMenuItem.Enabled = true;
                    connectToolStripMenuItem.Enabled = false;

                    if (String.IsNullOrWhiteSpace(textBox2.Text) || textBox2.Text == "")
                        name = "Client";

                    listBox1.Items.Add("Connected to " + client.RemoteEndPoint.ToString());
                    sendt = new Thread(Send);
                    recvt = new Thread(Receive);
                    sendt.IsBackground = true;
                    recvt.IsBackground = true;
                    sendt.Start();
                    recvt.Start();
                }
                catch (Exception ex)
                {
                    listBox1.Items.Add("No such server.");
                    client = null;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataEntered = true;
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sendt.Abort();

            if ((sender as String) != "this")
            {
                client.Send(Encoding.ASCII.GetBytes("QUIT"));
                recvt.Abort();
            }
            listBox1.Items.Add("Closed connection to server...");
            client.Shutdown(SocketShutdown.Both);
            client.Close();
            client = null;

            disconnectToolStripMenuItem.Enabled = false;
            connectToolStripMenuItem.Enabled = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (client!=null)
            {
                sendt.Abort();
                recvt.Abort();
                client.Send(Encoding.ASCII.GetBytes("QUIT"));
                client.Shutdown(SocketShutdown.Both);
                client.Close();
                client = null;
            }
            this.Close();
        }
    }
}
