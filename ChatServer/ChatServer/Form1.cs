﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace ChatServer
{
    public partial class Form1 : Form
    {
        private int port;
        private string name="Server",data;
        private Socket sock=null,client=null;
        private Thread sendt,recvt,ctc;
        private bool DataEntered = false;

        public Form1()
        {
            InitializeComponent();
            Icon i = new Icon(@"D:\Icons\FauxS-XP(Onyx)V3 Icon 03.ico");
            this.Icon = i;
            this.AcceptButton = button1;
            button1.Click+=new EventHandler(button1_Click);
            textBox1.Focus();
            textBox2.LostFocus+=new EventHandler(textBox2_LostFocus);
            exitToolStripMenuItem.Click+=new EventHandler(exitToolStripMenuItem_Click);
            disconnectToolStripMenuItem.Enabled = false;
            closeToolStripMenuItem.Enabled = false;
        }

        public void button1_Click(object sender, EventArgs e)
        {
            DataEntered = true;
        }

        public void Send()
        {
//            client.Send(Encoding.ASCII.GetBytes(name));
            while (true)
            {
                if (DataEntered)
                {
                    data = textBox1.Text.ToString();
                    client.Send(Encoding.ASCII.GetBytes(name + ": "+data));
                    listBox1.Items.Add(name + ": " + data);
                    DataEntered = false;
                    textBox1.Clear();
                }
            }
        }

        public void Receive()
        {
            int n;
            byte[] buf = new byte[1024];
/*            client.Receive(buf);
            clientname = Encoding.ASCII.GetString(buf);
            if (string.IsNullOrWhiteSpace(clientname) || clientname == "")
                clientname = "Client";*/
            do
            {
                n = client.Receive(buf);
                data = Encoding.ASCII.GetString(buf, 0, n);
                if(data.IndexOf("QUIT")<0)
                  listBox1.Items.Add(data);
            } while (data.IndexOf("QUIT")<0);
            closeToolStripMenuItem_Click("this", new EventArgs());
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (client != null)
            {
                listBox1.Items.Add("Please close connection to client first.");
            }
            else
            {
                if (sock!=null)
                {
                    sock.Close();
                    sock.Dispose();
                    sock = null;
                    ctc.Abort();
                }
                this.Close();
            }
        }

        private void ConnectTotClient()
        {
            byte[] buf=new byte[50];
            while (true)
            {
                sock.Listen(10);
                client = sock.Accept();

                listBox1.Items.Add("Connected to " + client.RemoteEndPoint.ToString());

                disconnectToolStripMenuItem.Enabled = false;
                closeToolStripMenuItem.Enabled = true;
                sendt = new Thread(new ThreadStart(Send));
                recvt = new Thread(new ThreadStart(Receive));
                sendt.IsBackground = true;
                recvt.IsBackground = true;

                sendt.Start();
                recvt.Start();

                ctc.Suspend();
            }
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            startToolStripMenuItem.Enabled = false;
            disconnectToolStripMenuItem.Enabled = true;
            PortPrompt pp = new PortPrompt();
            pp.ShowDialog();
            if (port != PortPrompt.port)
            {
                port = PortPrompt.port;
                sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                sock.Bind(new IPEndPoint(IPAddress.Any, port));
                listBox1.Items.Add("Server started on port " + port.ToString() + "...");
                ctc = new Thread(new ThreadStart(ConnectTotClient));
                ctc.IsBackground = true;
                ctc.Start();
            }
        }

        private void textBox2_LostFocus(object sender, EventArgs e)
        {
            if (name != textBox2.Text.ToString() && !string.IsNullOrEmpty(textBox2.Text))
            {
                name = textBox2.Text.ToString();
                listBox1.Items.Add("Name changed to " + name);
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sendt.Abort();

            if ((sender as string) != "this")
            {
                client.Send(Encoding.ASCII.GetBytes("QUIT"));
                recvt.Abort();
            }
            client.Shutdown(SocketShutdown.Both);
            client.Close();
            client.Dispose();
            client = null;
            listBox1.Items.Add("Waiting for connections...");

            disconnectToolStripMenuItem.Enabled = true;
            closeToolStripMenuItem.Enabled = false;

            ctc.Resume();
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            disconnectToolStripMenuItem.Enabled = false;
            startToolStripMenuItem.Enabled = true;

            sock.Close();
            sock.Dispose();
            sock = null;
            ctc.Abort();
            listBox1.Items.Add("Server disconnected from port " + port.ToString() + ".");
        }    
    }
}
