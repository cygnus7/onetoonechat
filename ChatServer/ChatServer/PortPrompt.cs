﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ChatServer
{
    public partial class PortPrompt : Form
    {
        public static int port;

        public PortPrompt()
        {
            InitializeComponent();
            this.AcceptButton = button1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox1.Text.ToString()) || string.IsNullOrWhiteSpace(textBox1.Text.ToString()) || Int32.Parse(textBox1.Text) <= 1024)
                MessageBox.Show("Invalid port number. Enter a port number greater than 1024.", "Invalid Port");
            else
            {
                port = Int32.Parse(textBox1.Text.ToString());
                this.Close();
            }
        }
    }
}
